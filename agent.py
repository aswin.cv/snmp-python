
import socket
import json
import sys

from threading import Thread, Timer 

class Agent():
    def __init__(self, oid_prefix="1.3.6.1.2.1.1."):
        self._bind_ip = '0.0.0.0'
        self._bind_port = 3001
        self._trap_port = 3002
        
        self._socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self._socket.bind((self._bind_ip, self._bind_port))
        self._address = None 
        
        self._oid_prefix = oid_prefix
        self._current_oid = None
        self._config = {'1.3.6.1.2.1.1.1.1' : 0 , '1.3.6.1.2.1.1.1.2' : "empty"}
        self._key_oid_pair = {'sampleInteger' : '1.3.6.1.2.1.1.1.1', 'sampleString' : '1.3.6.1.2.1.1.1.2'}
        self._trap_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

        self._trap_timer = None
        self._trap_timeout = 60

        self._ack = None
    
    def _do_start_trap_check_timer(self, trap_timeout=None):
        if trap_timeout:
            self._trap_timer = Timer(trap_timeout, self._do_trap_timeout, None)
        else:
            self._trap_timer = Timer(self._trap_timeout, self._do_trap_timeout, None)

        self._trap_timer.start()
    
    def _do_trap_timeout(self):
        print("sending trap")
        message = "trap "
        for oid in self._config:
                message += oid+" "+str(self._config[oid])
        if self._ack:
            self._do_start_trap_check_timer(40)
            self._ack =None
        else:
            self._do_start_trap_check_timer(20)
            self._trap_socket.sendto(message.encode(),(self._bind_ip,self._trap_port))
        

    def _receive_request_from_manager(self):
        request, self._address = self._socket.recvfrom(1024)
        print(request)
        return json.loads(request.decode())

    def _send_to_manager(self, message=None):
        if message:
            if self._socket:
                self._socket.sendto(json.dumps(message).encode(), self._address)
    
    def _request_handler(self):
        request = self._receive_request_from_manager()
        message = ""
        if request:
            if request['operation'] == 1:
                for key in self._key_oid_pair:
                    if request['key'] == key:
                        self._current_oid = self._key_oid_pair[request['key']]
                        break
                    elif request['key'] == self._key_oid_pair[key]:
                        self._current_oid = request['key']
                        break
                if self._current_oid:
                    message = self._current_oid+' '+str(self._config[self._current_oid])
                else:
                    message = "invalid oid"
            
            elif request['operation'] == 2:
                self._current_oid = self._oid_prefix + str(int(self._current_oid[-1])+1)
                try:
                    message = self._current_oid+' '+str(self._config[self._current_oid])
                except:
                    message = "no more oid"
                    self._current_oid = self._oid_prefix + str(int(self._current_oid[-1])-1)
            
            elif request['operation'] == 3 :
                for oid in self._config:
                    message += oid+" : "+str(self._config[oid])+", "
            
            elif request['operation'] == 4:
                flag = 0
                for key in list(self._key_oid_pair):
                    if request['key'] == key:
                        self._current_oid = self._key_oid_pair[request['key']]
                        flag = 1
                        break
                    elif request['key'] == self._key_oid_pair[key]:
                        self._current_oid = request['key']
                        flag = 1
                        break
                if flag:
                    self._config[self._current_oid] = request['value']
                    message = "success"
                else:
                    message = "failed"

            elif request['operation'] == 5 :
                message = "Ack"
                self._ack = True 

            else:
                message = "unknown"
            print(message)
            if message:
                self._send_to_manager(str(message))

if __name__== "__main__":
    agent = Agent()
    agent._do_start_trap_check_timer()
    # try:
    while True:
        agent._request_handler()
    # except Exception as e:
    #     agent._socket.close()
    #     print(e)
    #     sys.exit(0)


            
            



    


