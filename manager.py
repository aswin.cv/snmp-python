import socket
import json
from threading import Thread
from queue import Queue

class TrapHandler(Thread):
    def __init__(self, agent='0.0.0.0', port=3002, manager=None):
        self._agent = agent
        self._port = port
        self._manager = manager

        self._socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self._socket.bind((self._agent, self._port))
        Thread.__init__(self)

    def _receive_messages(self):
        while True:
            trap = self._socket.recvfrom(1024)
            if self._manager._trap_q.empty():
                print("\ngot a new trap from agent\n\n>>")
                self._manager._trap_q.put(trap)

    def run(self):
        self._receive_messages()


class Manager(Thread):
    def __init__(self, agent='0.0.0.0', port=3001):
        self._agent = agent
        self._port = port
        self._socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

        self._trap_q = Queue()
        Thread.__init__(self)

    def _send_to_agent(self, message=None):
        if message:
            self._socket.sendto(json.dumps(message).encode(),(self._agent, self._port))
        response = self._get_response_from_agent()
        return response
    
    def _get_response_from_agent(self):
        response, addr = self._socket.recvfrom(1024)
        response = json.loads(response.decode())
        return response
    
    def _trap_handler(self, message, addr):
        print("\n\ntrap from",addr)
        print(message,"\n\n")

    def _operation_handler(self):
        print('\nmib-prefix : 1.3.6.1.2.1.1.1\navailabele operations\nGetRequest\nGetNextRequest\nGetBulkRequest\nSetRequest\nInformRequest\n')
        operation = input(">>").split(" ")
        message = None
        if operation[0] == "GetRequest":
            message = {"operation" : 1, "key": operation[1]}
        elif operation[0] == "GetNextRequest":
            message = {"operation" : 2}
        elif operation[0] == "GetBulkRequest":
            message = {"operation" : 3}
        elif operation[0] == "SetRequest":
            message = {"operation" : 4, "key": operation[1], "value": operation[2]}
        elif operation[0] == "InformRequest":
            message = {"operation" : 5 }
        print(message)
        response = self._send_to_agent(message)
        if response:
            print(response)
        else:
            print("No response from agent")
        
    def run(self):
        while True:
            if not self._trap_q.empty():
                message, addr = self._trap_q.get()
                self._trap_handler(message,addr)
            self._operation_handler()

if __name__ == "__main__":
    server = input('server : ')
    port = int(input('port : '))
    manager = Manager(server,port)
    trap_handler = TrapHandler(manager=manager)
    trap_handler.start()
    manager.start()


    
        